#pragma once

#include "target.h"
#include "position.h"
#include "parameters.h"
#include <queue>

namespace Trajectory{
	class TargetSequence{
		public:
			TargetSequence(float _dt):lastPosition(0,0,0),timestamp(0),dt(_dt){}
			~TargetSequence(){}

			void push(const Target &t){
				targetQueue.push(t);
			}

			void pushDifference(Position diff, MotionFunctor *mf, const Parameters p){
				Position dest=lastPosition+diff;
				push(dest, mf, p);
			}

			void push(Position dest, MotionFunctor *mf, const Parameters p){
				if(lastPosition.angle>3.1415927f){
					lastPosition.angle-=2.f*3.1415927f;
				} else if(lastPosition.angle<=-3.1415927f){
					lastPosition.angle+=2.f*3.1415927f;
				}
				if(dest.angle>3.1415927f){
					dest.angle-=2.f*3.1415927f;
				} else if(dest.angle<=-3.1415927f){
					dest.angle+=2.f*3.1415927f;
				}
				if(lastPosition.angle-dest.angle>3.1415927f){
					dest.angle+=2.f*3.1415927f;
				} else if(lastPosition.angle-dest.angle<=-3.1415927f){
					dest.angle-=2.f*3.1415927f;
				}
				mf->setPosition(lastPosition, dest);
				mf->initialise();
				mf->configureParams(p.v0,p.vf,p.vmax,p.amax);
				targetQueue.push(Target(mf,dt));
				lastPosition.setVars(dest.x, dest.y, dest.angle);
			}

			inline bool isEmpty() const{
				return targetQueue.empty();
			}

			uint16_t getTimestamp() const{
				return timestamp;
			}

			Position getCurrentPosition(){
				timestamp++;
				Position p=targetQueue.front().getCurrentPosition(timestamp);
				if(!targetQueue.empty() && timestamp>=(targetQueue.front().getTime())){
					timestamp=0;
					targetQueue.front().destroy();
					targetQueue.pop();
				}
				return p;
			}

			Position getLastPosition(){
				return lastPosition;
			}

		private:
			std::queue<Target> targetQueue;
			Position lastPosition;
			uint16_t timestamp;
			float dt;
	};
}
