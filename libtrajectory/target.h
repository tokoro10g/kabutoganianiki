#pragma once

#include "position.h"
#include "functors.h"

namespace Trajectory {

	class Target {
		private:
			unsigned short time;
			MotionFunctor *mf;

		public:
			Target(MotionFunctor *_mf,float dt):mf(_mf){
				time=(unsigned short)(mf->getTime()/dt);
			}
			void destroy(){ mf->destroy(); delete mf; }

			Position getCurrentPosition(unsigned short t) const {
				float s=(float)t/(float)time;
				return mf->func(s);
			}

			MotionFunctor *getMF() const { return mf; }

			unsigned short getTime() const { return time; }
	};

}
