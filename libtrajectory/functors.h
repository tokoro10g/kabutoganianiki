#pragma once

#include "position.h"
#include <iostream>

namespace Trajectory {

	/*
	 * Base functors
	 * */

	class EasingFunctor{
		public:
			EasingFunctor(){}
			virtual ~EasingFunctor(){}
			virtual float func(float s) const = 0;
			virtual void initialise(){}
			virtual float configureParams(float v0,float vf,float vmax,float amax) = 0;
	};

	class MotionFunctor{
		protected:
			Position origin;
			Position dest;
			EasingFunctor *e;
			float time;
		public:
			MotionFunctor(){}
			MotionFunctor(Position _origin,Position _dest,EasingFunctor *_e):origin(_origin),dest(_dest),e(_e){}
			MotionFunctor(EasingFunctor *_e):e(_e){}
			MotionFunctor(const MotionFunctor& obj){
				origin=obj.origin; dest=obj.dest; e=obj.e;
			}
			virtual ~MotionFunctor(){}
			void destroy(){ delete e; }

			float getTime() const{ return time; }
			Position getOrigin() const { return origin; }
			Position getDest() const { return dest; }
			EasingFunctor *getE() const { return e; }
			void setPosition(Position _origin,Position _dest){
				origin=_origin;
				dest=_dest;
			}
			void configureParams(float v0,float vf,float vmax,float amax){
				float d=distance();
				time=e->configureParams(v0/d,vf/d,vmax/d,amax/d);
			}
			virtual Position func(float s) const = 0;
			virtual void initialise(){}
			virtual float distance() const = 0;
	};

	/*
	 * Derived functors
	 * */

	class EasingPoly5 : public EasingFunctor{
		private:
			float a,b,c,d,e;
		public:
			EasingPoly5():EasingFunctor(){}
			EasingPoly5(const EasingPoly5& obj){
				a=obj.a; b=obj.b; c=obj.c; d=obj.d; e=obj.e;
			}
			~EasingPoly5(){}
			float func(float s) const{
				return a*s*s*s*s*s+b*s*s*s*s+c*s*s*s+d*s*s+e*s;
			}
			float configureParams(float v0,float vf,float vmax,float amax){
				float t=1.875/(vmax+0.363*(v0+vf));
				float v1,v2; if(vf>=v0){ v1=v0; v2=vf; } else { v1=vf; v2=v0; }
				if(5.77/t-3.82*v1-1.83*v2<=amax){
					float ev0=v0*t; float evf=vf*t;
					a=-3.f*(ev0+evf)+6.f;
					b=8.f*ev0+7.f*evf-15.f;
					c=-6.f*ev0-4.f*evf+10.f;
					d=0;
					e=ev0;
				} else {
					t=5.77/(amax+3.82*v1+1.83*v2);
					float ev0=v0*t; float evf=vf*t;
					a=-3.f*(ev0+evf)+6.f;
					b=8.f*ev0+7.f*evf-15.f;
					c=-6.f*ev0-4.f*evf+10.f;
					d=0;
					e=ev0;
				}
				return t;
			}
	};

	class EasingLinear : public EasingFunctor{
		public:
			EasingLinear():EasingFunctor(){}
			~EasingLinear(){}
			float func(float s) const{
				return s;
			}
			float configureParams(float v0,float vf,float vmax,float amax){
				return 1.0/v0;
			}
	};

	class MotionLinear : public MotionFunctor{
		private:
			Position diff;
		public:
			MotionLinear(EasingFunctor *_e):MotionFunctor(_e){}
			MotionLinear(Position _origin,Position _dest,EasingFunctor *_e):MotionFunctor(_origin,_dest,_e){
				diff=dest-origin;
			}
			MotionLinear(const MotionLinear& obj){
				origin=obj.origin; dest=obj.dest; diff=dest-origin; e=obj.e;
			}
			~MotionLinear(){}
			Position func(float progress) const{
				float t=e->func(progress);
				return origin+(diff*t);
			}
			void initialise(){
				diff=dest-origin;
				e->initialise();
			}
			float distance() const{
				Position _diff(dest-origin);
				return sqrt(_diff.x*_diff.x+_diff.y*_diff.y+_diff.angle*_diff.angle);
			}
	};

	class MotionArc : public MotionFunctor{
		private:
			float r;
			float ox,oy;
		public:
			MotionArc(EasingFunctor *_e):MotionFunctor(_e){}
			MotionArc(Position _origin,Position _dest,EasingFunctor *_e):MotionFunctor(_origin,_dest,_e){
				initialise();
			}
			MotionArc(const MotionArc& obj){
				origin=obj.origin; dest=obj.dest; e=obj.e;
			}
			~MotionArc(){}
			Position func(float progress) const{
				// time factor calculated with an easing functor
				float t=e->func(progress);
				float alpha=(dest.angle-origin.angle)*t+origin.angle;
				return Position(ox-cos(alpha)*r,oy-sin(alpha)*r,alpha);
			}
			void initialise(){
				if(sin(origin.angle)-sin(dest.angle)<0.001){
					r=fabs((origin.x-dest.x)/(cos(dest.angle)-cos(origin.angle)));
				} else {
					r=fabs((origin.y-dest.y)/(sin(dest.angle)-sin(origin.angle)));
				}

				float adiff=dest.angle-origin.angle;
				if(adiff>3.1415927f) adiff-=2.f*3.1415927f;
				if(adiff<-3.1415927f) adiff+=2.f*3.1415927f;

				if(adiff>0) r=-r;

				ox=dest.x+cos(dest.angle)*r;
				oy=dest.y+sin(dest.angle)*r;
				e->initialise();
			}
			float distance() const{
				return fabs(r)*fabs(dest.angle-origin.angle);
			}
	};

}
