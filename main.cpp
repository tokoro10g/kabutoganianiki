#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/i2c.h"
#include "config/timing.h"
#include "config/stdperiph.h"

#include "fwlib/f4/stdperiph/inc/stm32f4xx_dbgmcu.h"

#include "machine/buzzer.h"
#include "machine/button.h"
#include "machine/machine.h"

#include "utils/debug.h"
#include "utils/mazeloader.h"
#include "utils/machinewallsource.h"
#include "utils/refmazewallsource.h"

#include "libtrajectory/parameters.h"
#include "libtrajectory/targetsequence.h"
#include "libtrajectory/position.h"
#include "libtrajectory/functors.h"

#include "libmazesolver/maze.h"
#include "libmazesolver/graph.h"
#include "libmazesolver/agent.h"

#include <vector>

using namespace stm32plus;
using namespace MazeSolver;

class Core {
	private:
		GpioC<DefaultDigitalInputFeature<GPIO_Speed_50MHz,Gpio::PUPD_NONE,0> > portc;

		Buzzer buzzer;
		EdgeButton button;

		Maze maze;
		Maze refMaze;
		Graph graph;

		MachineWallSource wallSource;
		RefMazeWallSource refWallSource;

		Agent agent;

		Machine machine;
	public:
		Core():
			button(portc[0]),
			wallSource(&machine),
			refWallSource(&refMaze),
			agent(&maze,&wallSource,&graph),
			machine()
	{ }
		void run(){
			using namespace Trajectory;
			MillisecondTimer::initialise();

			MazeLoader::loadEmpty(16, 16, 2, 2, maze);

			MazeLoader::loadTestMaze(16, 16, 2, 2, refMaze);

			graph.loadEmpty(16,16);
			graph.loadMaze(&maze);
			graph.resetCost();

			DBGMCU_Config(DBGMCU_STOP,ENABLE);
			DBGMCU_APB2PeriphConfig(DBGMCU_TIM8_STOP, ENABLE);

			MillisecondTimer::delay(1000);

			machine.initialise();

			debug<<"Setup done.\r\n";
			buzzer.playPowerOnSound();
			machine.pushTarget(
					Position(0,90,0), new MotionLinear(new EasingPoly5()), p_straight_start
					);

			std::vector<int16_t> v;
			int16_t index=15;
			float angle=0;
			agent.setIndex(15);
			agent.setDir(Maze::DirNorth);
			agent.reroute();
			button.updateState();
			v.push_back(index);
			Direction nextDir=agent.getDir();
			while(1){
				using namespace MyMath;
				using namespace MyMath::Machine;

				//machine.deactivate();

				button.updateState();
				if(button.getState()==EdgeButton::FallingEdge){
					buzzer.setFreqency(1300);
					buzzer.on();
					MillisecondTimer::delay(10);
					buzzer.off();
					machine.deactivate();
					debug.flush();
					for(int16_t i : v){
						debug<<i<<"\r\n";
					}
				} else if(button.getState()==EdgeButton::RisingEdge){
					buzzer.setFreqency(1900);
					buzzer.on();
					MillisecondTimer::delay(10);
					buzzer.off();
				}
				/*
				debug<<"left:"<<(uint16_t)machine.isSetWall(0x8);
				debug<<"front:"<<(uint16_t)machine.isSetWall(0x1);
				debug<<"right:"<<(uint16_t)machine.isSetWall(0x2)<<"\r\n";
				MillisecondTimer::delay(10);
				*/

				if(machine.isTargetSequenceEmpty()){
					if(agent.isInGoalCell(index,maze.getGoalNodeIndex())){
						continue;
					}
					if(machine.isSetWall(0x1)){
						buzzer.setFreqency(1600);
						buzzer.on();
						MillisecondTimer::delay(5);
						buzzer.off();
					}
					/*
					if(machine.isSetWall(0x2)){
						buzzer.setFreqency(2100);
						buzzer.on();
						MillisecondTimer::delay(10);
						buzzer.off();
					}
					if(machine.isSetWall(0x8)){
						buzzer.setFreqency(2400);
						buzzer.on();
						MillisecondTimer::delay(10);
						buzzer.off();
					}
					*/

					int16_t nextIndex=agent.stepMaze(maze.getGoalNodeIndex(), true);
					/*
					for(int i : graph.getRoute(maze.getGoalNodeIndex(),index)){
						debug<<"\troute:"<<(int16_t)i<<"\r\n";
					}
					*/
					debug<<"index:"<<nextIndex<<"\r\n";
					if(nextIndex<0){
						buzzer.setFreqency(1300);
						buzzer.on();
						MillisecondTimer::delay(50);
						buzzer.off();
						machine.deactivate();
						continue;
					}
					debug<<index<<" to "<<nextIndex<<","<<(int16_t)nextDir.half<<" to "<<(int16_t)agent.getDir().half<<"\r\n";
					if(nextIndex==index||agent.isPullBack(index,nextIndex,nextDir,agent.getDir())){
						// TODO: perform wall position calibration here
						debug<<"\tenter\r\n";
						machine.pushTargetDiff(Position(-90.f*sin(angle),90.f*cos(angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_end);
						buzzer.setFreqency(800);
						buzzer.on();
						MillisecondTimer::delay(300);
						buzzer.off();
						debug<<"\tturn\r\n";
						machine.pushTargetDiff(Position(0,0,PI), new MotionLinear(new EasingPoly5()), p_miniturn);
						angle+=PI;
						normalize(angle,PI);
						debug<<"\tout\r\n";
						machine.pushTargetDiff(Position(-90.f*sin(angle),90.f*cos(angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_start);
						nextDir=agent.getDir();
						continue;
					}
					nextDir=agent.getDir();
					Coord c=agent.getCoord();
					Position p(c.x*180,c.y*180,0);
					p.angle=-(float)(nextDir.half==0x2)*PI/2.f-(float)(nextDir.half==0x4)*PI+(float)(nextDir.half==0x8)*PI/2.f;
					p.x+=(nextDir.half==0x2)*90-(nextDir.half==0x8)*90;
					p.y+=(nextDir.half==0x1)*90-(nextDir.half==0x4)*90;
					if(fabs(angle-p.angle)<0.01f){
						debug<<"\tstraight\r\n";
						machine.pushTarget(p, new MotionLinear(new EasingPoly5()), p_straight);
					} else {
						debug<<"\tturn\r\n";
						Position din(-40.f*sin(angle),40.f*cos(angle),0.f);
						machine.pushTargetDiff(din, new MotionLinear(new EasingLinear()), p_turn);
						Position dout(40.f*sin(p.angle),-40.f*cos(p.angle),0.f);
						machine.pushTarget(p+dout, new MotionArc(new EasingLinear()), p_turn);
						machine.pushTarget(p, new MotionLinear(new EasingLinear()), p_turn);
					}
					if(agent.isInGoalCell(nextIndex, maze.getGoalNodeIndex())){
						debug<<"\tend\r\n";
						machine.pushTargetDiff(Position(-90.f*sin(p.angle),90.f*cos(p.angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_end);
					}
					angle=p.angle;
					index=nextIndex;
					v.push_back(index);
				}
			}
		}
};

int main() {
	SystemInit();
	SystemCoreClockUpdate();
	RCC_APB1PeriphResetCmd(RCC_APB1Periph_WWDG, DISABLE);
	Core core;
	core.run();
}
