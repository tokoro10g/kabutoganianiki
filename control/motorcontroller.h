#include "../machine/motor.h"
#include "../machine/encoder.h"
#include "pidcontroller.h"

using namespace stm32plus;

template<typename T, class TMotor, class TEncoder>
class MotorController : public TMotor, public TEncoder {
public:
	MotorController(T Kp,T Ki,T Kd,T isat=0):TMotor(),TEncoder(),pid(Kp,Ki,Kd,isat){}
	~MotorController(){}
	void update(T r){
		// NOTE: should be called after captureSpeed
		int16_t x=TEncoder::getSpeed();
		pid.update(r,x);
		TMotor::setOutput(pid.getOutput());
	}
private:
	PIDController<T> pid;
};
