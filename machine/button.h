#pragma once

#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/button.h"

using namespace stm32plus;

class EdgeButton : public PushButton {
	public:
		enum InternalState {
			High,
			Low,
			RisingEdge,
			FallingEdge
		} _internalState;

		EdgeButton(const GpioPinRef& pin):PushButton(pin,true){
			_internalState=High;
		}
		void updateState(){
			if(_internalState==FallingEdge || _internalState==RisingEdge){
				if(PushButton::getState()==PushButton::Pressed){
					_internalState=High;
				} else {
					_internalState=Low;
				}
			} else if(_internalState==High && PushButton::getState()==PushButton::NotPressed){
				_internalState=FallingEdge;
			} else if(_internalState==Low && PushButton::getState()==PushButton::Pressed){
				_internalState=RisingEdge;
			}
		}
		InternalState getState(){
			return _internalState;
		};
};
