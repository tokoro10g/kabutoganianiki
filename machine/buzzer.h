#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timing.h"
#include "config/timer.h"

using namespace stm32plus;

class Buzzer : public Timer10<
			   Timer10InternalClockFeature,
			   TimerChannel1Feature<>,
			   Timer10GpioFeature<
						TIMER_REMAP_NONE,
						TIM10_CH1_OUT> > {
	public:
		enum{
			MAX_COMPARE=200,
			VOLUME_COMPARE=20
		};
		Buzzer():Timer10(){
			this->setTimeBaseByFrequency(20000000, MAX_COMPARE);
			TimerChannel1Feature<>::initCompareForPwmOutput();
			this->enablePeripheral();
		}
		inline void on(){
			TimerChannel1Feature<>::setCompare(VOLUME_COMPARE);
		}
		inline void off(){
			TimerChannel1Feature<>::setCompare(0);
		}
		inline void setFreqency(uint16_t freq){
			this->setTimeBaseByFrequency(freq*MAX_COMPARE, MAX_COMPARE);
		}
		inline void playPowerOnSound(){
			setFreqency(1300);
			on();
			MillisecondTimer::delay(40);
			off();
			MillisecondTimer::delay(40);
			setFreqency(1300);
			on();
			MillisecondTimer::delay(40);
			off();
		}
	private:
};
