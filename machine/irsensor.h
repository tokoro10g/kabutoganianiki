#pragma once

#include "config/stm32plus.h"
#include "config/adc.h"

using namespace stm32plus;

class IRSensor {
public:
	IRSensor():dmaReady(false){
		dma.DmaInterruptEventSender.insertSubscriber(
				DmaInterruptEventSourceSlot::bind(this , &IRSensor::onAdcComplete)
				);
		dma.setNvicPriorities(3);
		dma.enableInterrupts(Adc1DmaChannelInterruptFeature::COMPLETE);

		dma.beginRead(buffer, 8);
		adc.startRegularConversion();
	}
	~IRSensor(){}

	bool isDmaReady() const{
		return dmaReady;
	}
	void clearDmaReady(){
		dmaReady=false;
	}
	uint16_t getLB() const{
		return buffer[0]+buffer[4];
	}
	uint16_t getRB() const{
		return buffer[1]+buffer[5];
	}
	uint16_t getLF() const{
		return buffer[2]+buffer[6];
	}
	uint16_t getRF() const{
		return buffer[3]+buffer[7];
	}

private:
	uint16_t buffer[8];
	Adc1DmaChannel<AdcMultiDmaMode1Feature<Adc1PeripheralTraits>, Adc1DmaChannelInterruptFeature> dma;
	Adc1<
		AdcClockPrescalerFeature<2>,
		AdcResolutionFeature<12>,
		Adc1Cycle15RegularChannelFeature<0,2,0,2>,
		AdcScanModeFeature<>,
		AdcContinuousModeFeature,
		AdcDualRegularSimultaneousDmaMode1Feature<Adc2<
			AdcClockPrescalerFeature<2>,
			AdcResolutionFeature<12>,
			Adc2Cycle15RegularChannelFeature<1,3,1,3>,
			AdcScanModeFeature<>,
			AdcContinuousModeFeature
			>, 20>
		> adc;
	bool dmaReady;
	void onAdcComplete(DmaEventType det){
		if(det==DmaEventType::EVENT_COMPLETE){
			dmaReady=true;
		}
	}
};
