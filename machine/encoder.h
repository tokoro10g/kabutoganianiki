#pragma once

#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timer.h"

#include "../utils/mymath.h"

namespace stm32plus{
	using TimerEncL = Timer3< Timer3GpioFeature<TIMER_REMAP_PARTIAL2, TIM3_CH1_IN, TIM3_CH2_IN>, TimerEncoderFeature<EncoderCounterEdge::Input1, EncoderPolarity::Rising, EncoderPolarity::Rising> >;
	using TimerEncR = Timer4< Timer4GpioFeature<TIMER_REMAP_NONE, TIM4_CH1_IN, TIM4_CH2_IN>, TimerEncoderFeature<EncoderCounterEdge::Input1, EncoderPolarity::Rising, EncoderPolarity::Falling> >;

	template<class TimerEnc>
	class Encoder : public TimerEnc {
		public:
			Encoder():TimerEnc(),prevCounter(0){
				TimerEnc::initialiseEncoderCounter(65535);

				TimerEnc::setCounter(0);
				TimerEnc::enablePeripheral();
			}
			~Encoder(){}
			inline void captureSpeed(){
				uint16_t nowCounter=TimerEnc::getCounter();
				int32_t diff=nowCounter-prevCounter;
				prevCounter=nowCounter;
				if((uint32_t)abs(diff)>32767){
					speed=diff-(int32_t)MyMath::signof(diff)*65536;
				} else {
					speed=diff;
				}
			}
			int16_t getSpeed() const{
				return speed;
			}
		private:
			uint16_t prevCounter;
			int16_t speed;
	};

	class Encoders : public Encoder<TimerEncL>, public Encoder<TimerEncR> {
		public:
			Encoders():Encoder<TimerEncL>(),Encoder<TimerEncR>(){}
			~Encoders(){}
			inline uint32_t getCounterL() const{
				return Encoder<TimerEncL>::getCounter();
			}
			inline uint32_t getCounterR() const{
				return Encoder<TimerEncR>::getCounter();
			}
		private:
	};
}
