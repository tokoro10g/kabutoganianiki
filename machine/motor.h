#pragma once

#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timer.h"

namespace stm32plus{
	template<class ChannelA, class ChannelB>
	class Motor : public Timer8<
				   Timer8InternalClockFeature,
				   ChannelA,
				   ChannelB,
				   Timer8GpioFeature<
							TIMER_REMAP_NONE,
							TIM8_CH1_OUT,
							TIM8_CH2_OUT,
							TIM8_CH3_OUT,
							TIM8_CH4_OUT > > {
		public:
			enum{
				MAX_COMPARE=200
			};
			Motor():Timer8<
				   Timer8InternalClockFeature,
				   ChannelA,
				   ChannelB,
				   Timer8GpioFeature<
							TIMER_REMAP_NONE,
							TIM8_CH1_OUT,
							TIM8_CH2_OUT,
							TIM8_CH3_OUT,
							TIM8_CH4_OUT > >(){
				this->setTimeBaseByFrequency(20000000, MAX_COMPARE);
				ChannelA::initCompareForPwmOutput();
				ChannelB::initCompareForPwmOutput();
				this->enablePeripheral();
			}
			inline void setOutput(int16_t value){
				if(value>MAX_COMPARE) value=MAX_COMPARE;
				if(value<-MAX_COMPARE) value=-MAX_COMPARE;
				if(value>0){
					ChannelA::setCompare(MAX_COMPARE);
					ChannelB::setCompare(MAX_COMPARE-value);
				} else {
					ChannelA::setCompare(MAX_COMPARE+value);
					ChannelB::setCompare(MAX_COMPARE);
				}
			}
			inline void stop(){
				ChannelA::setCompare(0);
				ChannelB::setCompare(0);
			}
	};

	class Motors : public Timer8<
				   Timer8InternalClockFeature,
				   TimerChannel1Feature<>,
				   TimerChannel2Feature<>,
				   TimerChannel3Feature<>,
				   TimerChannel4Feature<>,
				   Timer8GpioFeature<
							TIMER_REMAP_NONE,
							TIM8_CH1_OUT,
							TIM8_CH2_OUT,
							TIM8_CH3_OUT,
							TIM8_CH4_OUT > > {
		public:
			enum{
				MAX_COMPARE=200
			};
			Motors():Timer8(){
				this->setTimeBaseByFrequency(20000000, MAX_COMPARE);
				TimerChannel1Feature<>::initCompareForPwmOutput();
				TimerChannel2Feature<>::initCompareForPwmOutput();
				TimerChannel3Feature<>::initCompareForPwmOutput();
				TimerChannel4Feature<>::initCompareForPwmOutput();
				this->enablePeripheral();
			}
			inline void setOutput(int16_t left,int16_t right){
				if(left>MAX_COMPARE) left=MAX_COMPARE;
				if(left<-MAX_COMPARE) left=-MAX_COMPARE;
				if(right>MAX_COMPARE) right=MAX_COMPARE;
				if(right<-MAX_COMPARE) right=-MAX_COMPARE;
				if(left>0){
					TimerChannel3Feature<>::setCompare(MAX_COMPARE);
					TimerChannel4Feature<>::setCompare(MAX_COMPARE-left);
				} else {
					TimerChannel3Feature<>::setCompare(MAX_COMPARE+left);
					TimerChannel4Feature<>::setCompare(MAX_COMPARE);
				}
				if(right>0){
					TimerChannel1Feature<>::setCompare(MAX_COMPARE);
					TimerChannel2Feature<>::setCompare(MAX_COMPARE-right);
				} else {
					TimerChannel1Feature<>::setCompare(MAX_COMPARE+right);
					TimerChannel2Feature<>::setCompare(MAX_COMPARE);
				}
			}
			inline void stopAll(){
				setOutput(0,0);
			}
		private:
	};
}
