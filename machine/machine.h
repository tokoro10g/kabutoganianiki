#pragma once

#include "../control/controlTimer.h"
#include "../control/motorcontroller.h"
#include "../control/astolfi.h"

#include "../devices/mpu6050.h"

#include "../utils/debug.h"
#include "../utils/mymath.h"
#include "../utils/mymath_machine.h"

#include "../libtrajectory/targetsequence.h"
#include "../libtrajectory/target.h"
#include "../libtrajectory/position.h"
#include "../libtrajectory/functors.h"

#include "./irsensor.h"
#include "./irled.h"

#include <cmath>
#include <queue>

using namespace stm32plus;

class Machine {
	private:
		enum{
			FRONT_THRESHOLD=2100,
			SIDE_THRESHOLD=2500
		};

		MotorController<
			float,
			Motor< TimerChannel3Feature<>, TimerChannel4Feature<> >,
			Encoder<TimerEncL>
			> motorL;
		MotorController<
			float,
			Motor< TimerChannel1Feature<>, TimerChannel2Feature<> >,
			Encoder<TimerEncR>
			> motorR;

		ControlTimer<
			Timer5< Timer5InternalClockFeature, Timer5InterruptFeature >,
			Machine
			> motorCtrlTimer;
		ControlTimer<
			Timer7< Timer7InternalClockFeature, Timer7InterruptFeature >,
			Machine
			> outerCtrlTimer;

		GpioB<DefaultDigitalOutputFeature<14,15> > portb;
		IRLED<GpioPinRef,GpioPinRef> irled;
		IRSensor ir;

		MPU6050<I2C2_Default<I2CSingleByteMasterPollingFeature>,0> mpu6050;

		struct State {
			/* State variables and inputs of Astolfi's controller */
			float x;
			float y;
			float phi;
			float rx;
			float ry;
			float rphi;
			float v;
			float w;
			State(int16_t _x,int16_t _y,int16_t _phi):
				x(_x),y(_y),phi(_phi),rx(_x),ry(_y),rphi(_phi),v(0),w(0){}
			State():x(0),y(0),phi(0),rx(0),ry(0),rphi(0),v(0),w(0){}
			void update(int16_t vl, int16_t vr){
				using namespace MyMath;
				using namespace MyMath::Machine;
				/*
				phi+=vl-vr;
				normalize(phi, PIInPulseDiff);
				*/
				float phiInRadian=convertPulseDiffToMachineRadian(phi);
				x+=(float)(vl+vr)/2.0*cos(PI/2.0-phiInRadian);
				y+=(float)(vl+vr)/2.0*sin(PI/2.0-phiInRadian);
			}
			void calculateOutput(int16_t &outputL,int16_t &outputR){
				outputL=v-w;
				outputR=v+w;
			}
		};
		State state;

		Astolfi<State> astolfi;

		Trajectory::TargetSequence targetSequence;

		bool activated;

		uint16_t irLF, irLB, irRF, irRB;

	public:
		Machine():
			motorL(7.5, 0.5, 0.3, 200), // Kp=7, Ki=1, Kd=0, isat=200
			motorR(7.5, 0.5, 0.3, 200), // Kp=7, Ki=1, Kd=0, isat=200
			motorCtrlTimer(this,1000,4), // 1000Hz = 1ms cycle
			outerCtrlTimer(this,200,5),  //  200Hz = 5ms cycle
			irled(portb[14],portb[15]),
			mpu6050(I2C::Parameters(400000)), // 400kHz
			state(),
			// Krho=0.025, Kphi=0.017, Kalpha=0.006
			// vsat=20, wsat=10
			//astolfi(0.007,-0.013,0.0018,150,100),
			//astolfi(0.013,-0.015,0.0018,150,100),
			astolfi(0.0145,-0.015,0.0018,150,100),
			targetSequence(0.005),
			activated(true)
		{ }
		~Machine(){}

		void initialise(){
			mpu6050.setTimeout(20);

			debug<<"Testing MPU6050...\r\n";
			while(!mpu6050.test());
			debug<<"MPU6050 test passed.\r\n";

			MillisecondTimer::delay(1000);

			debug<<"Setting up MPU6050...\r\n";
			mpu6050.setup();
			debug<<"complete.\r\n";
			MillisecondTimer::delay(1000);

			motorCtrlTimer.bind(&Machine::motorCtrlHandler);
			outerCtrlTimer.bind(&Machine::outerCtrlHandler);
			motorL.Encoder::setCounter(0);
			motorR.Encoder::setCounter(0);
			Nvic::initialise();
		}

		void activate(){
			activated=true;
		}
		void deactivate(){
			activated=false;
			motorL.stop();
			motorR.stop();
		}

		inline void pushTargetDiff(const Trajectory::Position& diff, Trajectory::MotionFunctor *mf, Trajectory::Parameters p){
			targetSequence.pushDifference(diff, mf, p);
		}

		inline void pushTarget(const Trajectory::Position& pos, Trajectory::MotionFunctor* mf, Trajectory::Parameters p){
			targetSequence.push(pos, mf, p);
		}

		void setReference(float rx,float ry,float rphi) volatile{
			using namespace MyMath;
			using namespace MyMath::Machine;
			state.rx=rx;
			state.ry=ry;
			normalize(rphi, PIInPulseDiff);
			state.rphi=rphi;
		}

		void printState() const {
			// debug output
			// TODO: fix stringstream to print floating point values
			uint16_t encL,encR;
			encL=motorL.Encoder::getCounter();
			encR=motorR.Encoder::getCounter();
			debug.flush();
			debug<<"x:"<<(int32_t)state.x<<"\r\n";
			debug<<"y:"<<(int32_t)state.y<<"\r\n";
			debug<<"phi:"<<(int32_t)state.phi<<"\r\n";
			debug<<"enc:"<<encL<<","<<encR<<"\r\n";
			debug.flush();
		}

		inline bool isTargetSequenceEmpty() const{
			return targetSequence.isEmpty();
		}

		inline bool isSetWall(uint8_t dir){
			if((dir&0xf)==0x1){
				if(irRF>SIDE_THRESHOLD) irRB-=(irRF-SIDE_THRESHOLD)/10;
				if(irLF>SIDE_THRESHOLD) irLB-=(irLF-SIDE_THRESHOLD)/10;
				return (irLB>FRONT_THRESHOLD && irRB>FRONT_THRESHOLD);
			} else if((dir&0xf)==0x2){
				return (irRF>SIDE_THRESHOLD);
			} else if((dir&0xf)==0x8){
				return (irLF>SIDE_THRESHOLD);
			}
			return false;
		}

		void motorCtrlHandler(TimerEventType te, uint8_t timerNumber){
			motorL.Encoder::captureSpeed();
			motorR.Encoder::captureSpeed();
			int16_t vl=motorL.Encoder::getSpeed();
			int16_t vr=motorR.Encoder::getSpeed();

			state.update(vl,vr);

			int16_t outputL,outputR;
			state.calculateOutput(outputL, outputR);

			// NOTE: should be called after Encoder::captureSpeed
			if(activated){
				motorL.update(outputL);
				motorR.update(outputR);
			}
		}

		void outerCtrlHandler(TimerEventType te, uint8_t timerNumber){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;

			// update current target
			if(!targetSequence.isEmpty()){
				Position p=convertRealPositionToMachinePosition(targetSequence.getCurrentPosition());
				//debug<<(int32_t)(convertPulseToWheelDistance(p.x))<<","<<(int32_t)(convertPulseToWheelDistance(p.y))<<","<<(int16_t)(convertPulseDiffToMachineDegree(p.angle))<<"\r\n";
				setReference(p.x, p.y, p.angle);
			}

			// read yaw rate from gyroscope
			float gyrz;
			// TODO: fix this magic number to be calibrated
			gyrz=(float)(mpu6050.readGyrZ()+8)/16.f;
			state.phi+=convertGyroValueToPulseDiff(gyrz);
			normalize(state.phi, PIInPulseDiff);

			try {
				astolfi.control(state);
			} catch(int e){
				if(e==-1){
					deactivate();
				}
			}

			irled.turnOn();
			ir.clearDmaReady();
			for(volatile uint8_t i=0;i<100;i++);
			irLF=ir.getLF();
			irLB=ir.getLB();
			irRF=ir.getRF();
			irRB=ir.getRB();
			//debug<<irLF<<","<<irRF<<"\r\n";
			//debug<<irLB<<","<<irRB<<"\r\n\r\n";
			irled.turnOff();

			// move onto next target
		}
};
