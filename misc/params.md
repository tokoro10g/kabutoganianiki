## Machine data
- Width: 70.5[mm]
- Tread Width: 59.5[mm]

### Motor
- Offset: 18/200
- Offset_optimal: 8/200
- Avg.500 50/200:  
70.214 67.882
- Avg.500 100/200:  
160.344 157.2
- Avg.500 150/200:  
247.046 238.704
- Avg.500 200/200:  
328.434 321.06
- Time constant: 50[ms]

### Gear
- Gear ratio: 1:4.5

### Wheel
- Diameter: 24.5[mm]
- Distance/Rotation: 76.96902[mm]
- Width: 11[mm]

### Encoder
- Pulse/Rotation: 500[P/R]
- Pulse/Rotation*4: 2000[P/R]
- Pulse/Rotation*4(Wheel): 9000[P/R]
- Distance/Pulse: 0.00855211[mm/P]
- Pulse/Distance: 116.930[P/mm]

## 
