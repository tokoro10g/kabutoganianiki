/* Accuracy test code of floating point number. */
/* from http://stackoverflow.com/a/507879 */
#include <stdio.h>
#include <cmath>

#define E_PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062

int main(int argc, char** argv)
{
	long double pild = E_PI;
	double pid = pild;
	float pif = pid;
	float pif_test = 3.1415926f;
	printf("%s\n%1.80f\n%1.80f\n%1.80f\n%1.80Lf\n",
			"3.14159265358979323846264338327950288419716939937510582097494459230781640628620899",
			pif_test, pif, pid, pild);
	return 0;
}
