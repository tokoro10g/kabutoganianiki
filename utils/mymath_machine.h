#pragma once

#include "mymath.h"
#include "../libtrajectory/position.h"

namespace MyMath { namespace Machine {
	/* Constants */
	static const float GearRatio = 4.5;
	static const uint16_t EncoderPulse = 1000;
	static const uint16_t WheelPulse = (uint16_t)(GearRatio*(float)EncoderPulse);

	static const float WheelDiameter = 24.5;
	static const float WheelPerimeter = WheelDiameter*PI;

	static const float WheelRadianPerPulse = PI/(WheelPulse/2);
	static const float PulsePerWheelRadian = 1.0/WheelRadianPerPulse;

	static const float WheelDegreePerPulse = 180.0/(WheelPulse/2);
	static const float PulsePerWheelDegree = 1.0/WheelDegreePerPulse;

	static const float WheelDistancePerPulse = WheelPerimeter/WheelPulse;
	static const float PulsePerWheelDistance = 1.0/WheelDistancePerPulse;

	static const float MachineWidth = 70.5;
	static const float WheelWidth = 11;
	static const float TreadWidth = MachineWidth-WheelWidth-1.35;

	static const float PulseDiffPerMachineRadian = TreadWidth*PulsePerWheelDistance;
	static const float MachineRadianPerPulseDiff = 1.0/PulseDiffPerMachineRadian;

	static const float MachineDegreePerPulseDiff = 180.0/(PI*TreadWidth*PulsePerWheelDistance);
	static const float PulseDiffPerMachineDegree = 1.0/MachineDegreePerPulseDiff;

	static const float GyroValuePerMachineRotation = 37385.0;

	static const float GyroValuePerMachineRadian = GyroValuePerMachineRotation/PI;
	static const float MachineRadianPerGyroValue = 1.0/GyroValuePerMachineRadian;

	static const float GyroValuePerMachineDegree = GyroValuePerMachineRotation/180;
	static const float MachineDegreePerGyroValue = 1.0/GyroValuePerMachineDegree;

	static const float GyroValuePerPulseDiff = GyroValuePerMachineRadian*MachineRadianPerPulseDiff;
	static const float PulseDiffPerGyroValue = 1.0/GyroValuePerPulseDiff;

	/* Unit conversion functions */
	inline float convertPulseToWheelRadian(int32_t pulse){
		return (float)pulse*WheelRadianPerPulse;
	}
	inline float convertPulseToWheelDegree(int32_t pulse){
		return (float)pulse*WheelDegreePerPulse;
	}
	inline float convertPulseToWheelDistance(int32_t pulse){
		return (float)pulse*WheelDistancePerPulse;
	}
	inline float convertWheelDistanceToPulse(float distance){
		return distance*PulsePerWheelDistance;
	}
	inline float convertPulseDiffToMachineRadian(int32_t diff){
		return (float)diff*MachineRadianPerPulseDiff;
	}
	inline float convertMachineRadianToPulseDiff(float radian){
		return radian*PulseDiffPerMachineRadian;
	}
	inline float convertPulseDiffToMachineDegree(int32_t diff){
		return (float)diff*MachineDegreePerPulseDiff;
	}
	inline float convertMachineDegreeToPulseDiff(float degree){
		return degree*PulseDiffPerMachineDegree;
	}
	inline float convertGyroValueToMachineRadian(int32_t gyro){
		return (float)gyro*MachineRadianPerGyroValue;
	}
	inline float convertMachineRadianToGyroValue(float radian){
		return radian*GyroValuePerMachineRadian;
	}
	inline float convertGyroValueToMachineDegree(int32_t gyro){
		return (float)gyro*MachineDegreePerGyroValue;
	}
	inline float convertMachineDegreeToGyroValue(float degree){
		return degree*GyroValuePerMachineDegree;
	}
	inline float convertGyroValueToPulseDiff(int32_t gyro){
		return (float)gyro*PulseDiffPerGyroValue;
	}

	inline const Trajectory::Position convertRealPositionToMachinePosition(Trajectory::Position rp){
		Trajectory::Position mp;
		mp.setVars(
				convertWheelDistanceToPulse(rp.x),
				convertWheelDistanceToPulse(rp.y),
				convertMachineRadianToPulseDiff(normalized(-rp.angle,PI))
				);
		return mp;
	}

	static const float PIInPulseDiff = convertMachineRadianToPulseDiff(PI);

}}
