#pragma once

inline int16_t u2i(uint8_t *u){
	return (int16_t)(((*u)<<8)|(*(u+1)));
}
inline uint16_t u2ui(uint8_t *u){
	return (uint16_t)(((*u)<<8)|(*(u+1)));
}
inline void f2u(float f,uint8_t *u){
	memcpy(u, &f, 4);
}
