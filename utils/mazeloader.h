#include "../libmazesolver/maze.h"

class MazeLoader {
	private:
		MazeLoader();
	public:
		static void loadEmpty(int w,int h,int x,int y,MazeSolver::Maze &maze){
			maze.resize(w,h);
			maze.setType(1);
			maze.setGoal(x,y);
			for(int i=0;i<h;i++){
				for(int j=0;j<w;j++){
					MazeSolver::CellData cell={0};
					if(j==0){
						cell.bits.WEST=1;
					} else if(j==w-1){
						cell.bits.EAST=1;
					}
					if(i==0){
						cell.bits.SOUTH=1;
					} else if(i==h-1){
						cell.bits.NORTH=1;
					}
					if(i==0&&j==0){
						cell.bits.EAST=1;
						cell.bits.CHK_EAST=1;
					}
					if(i==0&&j==1){
						cell.bits.WEST=1;
						cell.bits.CHK_WEST=1;
					}
					maze.setCell(i*w+j,cell);
				}
			}
		}
		static void loadTestMaze(int w,int h,int x,int y,MazeSolver::Maze &maze){
			maze.resize(w,h);
			maze.setType(1);
			maze.setGoal(x,y);
			static const char *data="effff8aed468aac2a8aa93a8915538444440";
			for(int i=0;i<6;i++){
				for(int j=0;j<6;j++){
					MazeSolver::CellData cell={0};
					char c=data[6*i+j];
					if(c>='0'&&c<='9'){
						c-='0';
					} else if(c>='a'&&c<='f'){
						c-='a';
						c+=10;
					}
					cell.half=c;
					maze.setCell(i*w+j,cell);
				}
			}
		}
};
